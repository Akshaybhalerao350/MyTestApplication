package bhaleraoakhay350.com.mytestapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityForResult2 extends AppCompatActivity implements View.OnClickListener {
        EditText etEdittext;
        Button btSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_result2);
        bindviews();
    }

    private void bindviews() {
        etEdittext=findViewById(R.id.editText1);
        btSubmit=findViewById(R.id.button1);
        btSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.button1:
                // Do something
                String message=etEdittext.getText().toString();
                Intent intent=new Intent();
                intent.putExtra("MESSAGE",message);
                setResult(2,intent);
                finish();//finishing activity
                break;   //handle multiple view click events
        }
    }
}
