package bhaleraoakhay350.com.mytestapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import bhaleraoakhay350.com.mytestapplication.Utils.ApplicationConstant;

public class StartActivityForResultDemo extends AppCompatActivity implements View.OnClickListener {
    TextView tvTextview;
    Button btnClick;
    View view;
    ViewGroup viewGroup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);//int flag, int mask
        setContentView(R.layout.startactivity_forresultdemo);
        bindviews();
    }

    private void bindviews() {

        tvTextview = findViewById(R.id.textview1);
        btnClick = findViewById(R.id.btnClick);
         viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
        btnClick.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClick:
                // Do something
                Intent intent=new Intent(StartActivityForResultDemo.this,ActivityForResult2.class);
                startActivityForResult(intent, 2);// Activity is started with requestCode 2
                break;   //handle multiple view click events
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==2)
        {
            String message=data.getStringExtra("MESSAGE");
            tvTextview.setText(message);
            new ApplicationConstant().Show_Toast(getApplicationContext(), viewGroup,"Your Email Id is Invalid.");
        }
    }
}